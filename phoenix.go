package goleshchat

import (
	"encoding/json"
	"errors"
)

func newPhoenixTuple(topic string, event string, payload interface{}) *phoenixTuple {
	return &phoenixTuple{
		Topic:   topic,
		Event:   event,
		Payload: payload,
	}
}

type phoenixTuple struct {
	Topic   string      // something I understand
	Event   string      // and this
	Payload interface{} // meh.
}

// I don't intend to re-implement the whole thing, so we get a To and From function so I can work with data in a sensible way
func (p *phoenixTuple) ToSlice() []interface{} {
	tuple := []interface{}{}

	var payload interface{}
	if p.Payload == nil {
		payload = "{}"
	} else {
		payload = p.Payload
	}

	// hardcoded "1" because I don't understand.
	tuple = append(tuple, "1")
	tuple = append(tuple, "1")
	tuple = append(tuple, p.Topic)
	tuple = append(tuple, p.Event)
	tuple = append(tuple, payload)

	return tuple
}

func (p *phoenixTuple) FromSlice(tuple []json.RawMessage) error {
	if len(tuple) != 5 {
		return errors.New("tuple is the wrong size. should be 5")
	}

	// we ignore the first two things.

	err := json.Unmarshal(tuple[2], &p.Topic)
	if err != nil {
		return err
	}

	err = json.Unmarshal(tuple[3], &p.Event)
	if err != nil {
		return err
	}

	p.Payload = tuple[4]

	return nil
}
