# Golesh Chat
> A Glimesh Chat Interface For Golang.

This was written fairly quickly, there's probably some issues in it,

Feel free to submit pull requests to make it work nicer.

**Also**, Glimesh's chat API is still in development, as is the platform in general. Expect things to change or get
added. This library will follow Go's compatibility policy and SemVer.

## Usage
1. Install with `go get gitlab.com/ponkey364/golesh-chat`
2. Set up a way of getting a token through OAuth
3. Call `goleshchat.New()` with your token and onMessage handler
4. Open the session with `session.Open()`
5. Connect to channels by calling `session.ConnectToChannel([id])` with an ID
6. When you're done, call `session.Close()`

### Example
```go
package main

import "gitlab.com/ponkey364/golesh-chat"

func main() {
    session := goleshchat.New("token", func(s *goleshchat.Session, m *goleshchat.ChatMessage) {
        if m.Message == "ping" {
            s.SendMessage("pong", m.Channel.ID)
        }
    })
    
    err := session.Open()
    if err != nil {
        panic(err)
    }
    
    session.ConnectToChannel(1140)
    
    // run for as long as you need

    session.Close()
}
```

## Debugging
If you want to see what is going on between this library and Glimesh, pull from the `[]interface{}` channel `session.Debug`, you'll get a direction `READ` or `WRITE`.  
Writing to this yields nothing, since nothing is watching it.